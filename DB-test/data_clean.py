import pandas as pd
import re
import docx
import os
from pathlib import Path
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(os.path.expanduser('~user'), '.env')
load_dotenv(dotenv_path)
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

def clean_orders(df_shop_orders):
    """
    cleaning orders csv file
    """
    #ORDERS DATA
    #Shopify
    df_shop_orders=df_shop_orders[['order_id','customer_id','email',
                                    'status','total_price','created_at','billing_province','line_item_variant_title',
                                    'line_item_purchase_item_type','discount_code','total_discounts','line_item_title']]
    df=df_shop_orders.groupby('order_id')['line_item_title'].agg(lambda x: x.tolist())
    df=pd.DataFrame(df)
    df_temp=df_shop_orders.drop_duplicates(subset=['order_id'])
    df_order=pd.merge(df_temp, df, left_on='order_id', right_on='order_id', how='left')
    df_order=df_order.rename(columns={'line_item_title_y':'item_list','line_item_variant_title':'plan_type','total_discounts':'discount_amount'})
    #df_order['coupon']=df_order['discount_code'].apply(lambda x: re.search(r'"code":(.*)"type"',x).group(1) if pd.Series(x).str.contains(r"code").sum()>0 else "")
    df_order=df_order.drop(['line_item_purchase_item_type','status','discount_code','line_item_title_x'], axis = 1)
    return df_order


def clean_subs(df_subs_shopify):
    """
    cleaning subs csv file
    """
    #SUBS DATA
    #Shopify
    df_subs_shopify=df_subs_shopify[['customer_id','created_at','customer_email','status',
                                    'recurring_price','cancelled_at','cancellation_reason',
                                    'cancellation_reason_comments','product_title']]
    df=df_subs_shopify.groupby('customer_id')['product_title'].agg(lambda x: x.tolist())
    df=pd.DataFrame(df)
    df_temp=df_subs_shopify.drop_duplicates(subset=['customer_id'])
    df_subs=pd.merge(df_temp, df, left_on='customer_id', right_on='customer_id', how='left')
    df_subs=df_subs.rename(columns={'customer_email':'email','status':'sub_status',
                                                    'recurring_price':'plan_price',
                                                    'product_title_y':'product_list'})
    df_subs=df_subs.drop(['product_title_x'], axis = 1)
    return df_subs


if __name__ == "__main__":

    Path('data\\').mkdir(parents=True, exist_ok=True)

    df_order_1=pd.read_csv(ROOT_DIR + '/data/Raw/orders_1.csv')
    df_order_2=pd.read_csv(ROOT_DIR + '/data/Raw/orders_2.csv')
    df_subs=pd.read_csv(ROOT_DIR + '/data/Raw/subs.csv')

    #concat orders 1-3
    df_orders = pd.concat([df_order_1, df_order_2], ignore_index=True, sort=False)
    df_orders=df_orders.drop_duplicates()

    #orders data
    df_orders=clean_orders(df_orders)
    #subs data
    df_subs=clean_subs(df_subs)

    #export data to cleaned
    df_orders.to_excel(ROOT_DIR + '/data/Cleaned/orders.xlsx',index=False)
    df_subs.to_excel(ROOT_DIR + '/data/Cleaned/subs.xlsx',index=False)
    print('Export Completed!')
