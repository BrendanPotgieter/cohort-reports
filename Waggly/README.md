# Waggly Cohort Report Data Extraction:
Extract & Clean Data from upcoming charges csv file. This data will be used to update Waggly Cohort Report.

# Installation on Local Python env
* Clone repo
* Install requirements `pip install -r requirements.txt`
* Download csv's from Shopify & Chargebee and add to `data` folder in repo
* Rename shopify csv's to `subs_shopify.csv` & `shopify_orders.csv`
* Run app `python data_clean.py`

# How to download Shopify CSV's?
* Download all processed orders from recharge - create export and export all `processed orders` from `2021-04-01` onwards.
* Download all subscribers from recharge - create export and export `subscribers - all`

# How to download Chargebee CSV's?
* Export all subscriptions
* Export all invoices
* From invoices folder extract `invoices.csv`, `Coupons.csv` & `SubscriptionCoupons.csv`

# Who do I talk to?
* brendan.potgieter@madpaws.com.au
* brendanpotgieter12@gmail.com
