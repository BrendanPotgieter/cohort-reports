import pandas as pd
import re
import docx
import os
from pathlib import Path
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(os.path.expanduser('~user'), '.env')
load_dotenv(dotenv_path)
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

def clean_orders(df_shop_orders,df_order_cb,df_subscoup_cb,df_coup_type_cb,df_subs_cb):
    """
    cleaning orders csv file
    """
    #ORDERS DATA
    #Shopify
    df_shop_orders=df_shop_orders[['order_id','customer_id','line_item_subscription_id','email','status','total_price','created_at','billing_province','line_item_variant_title', 'line_item_title','line_item_purchase_item_type','discount_code','total_discounts']]
    df_temp=df_shop_orders.groupby('order_id')['line_item_title'].agg(lambda x: x.tolist())
    df_temp=pd.DataFrame(df_temp)
    df_shop_orders=df_shop_orders[df_shop_orders['status']=='SUCCESS']
    df_shop_orders=df_shop_orders[df_shop_orders['line_item_purchase_item_type']=='SUBSCRIPTION']
    df_shop_orders=df_shop_orders.rename(columns={'line_item_subscription_id':'subscription_id','line_item_variant_title':'plan_type','total_discounts':'discount_amount'})
    df_shop_orders['discount_code']=df_shop_orders['discount_code'].apply(lambda x: str(x))
    df_shop_orders['coupon']=df_shop_orders['discount_code'].apply(lambda x: re.search(r'"code":(.*)"type"',x).group(1) if pd.Series(x).str.contains(r"code").sum()>0 else "")
    df_shop_orders=df_shop_orders.drop(['line_item_purchase_item_type','status','discount_code', 'line_item_title'], axis = 1)
    df_shop_orders=pd.merge(df_shop_orders, df_temp, left_on='order_id', right_on='order_id', how='left')
    df_shop_orders=df_shop_orders.rename(columns={'line_item_title':'offer_type'})
    #Chargebee
    df_order_cb=df_order_cb[['Invoice Number','Customer Id','Subscription Id','Customer Email','Status','Amount','Invoice Date']]
    df_subscoup_cb=df_subscoup_cb[['Subscription Id','Coupon Id']]
    df_coup_type_cb=df_coup_type_cb[['Coupon Id','Name','Discount Amount']]
    df_coupon_cb=pd.merge(df_subscoup_cb,df_coup_type_cb,on='Coupon Id',how='left')
    df_order_cb=pd.merge(df_order_cb,df_coupon_cb,on='Subscription Id',how='left')
    df_sub_plan=df_subs_cb[['subscriptions.id','subscriptions.plan_id','addresses.state']]
    df_sub_plan=df_sub_plan.rename(columns={'subscriptions.id':'Subscription Id','subscriptions.plan_id':'Plan Id','addresses.state':'Billing Province'})
    df_order_cb=pd.merge(df_order_cb,df_sub_plan,on='Subscription Id',how='left')
    df_order_cb=df_order_cb[df_order_cb['Status']=='Paid']
    df_order_cb=df_order_cb.drop(['Status','Coupon Id', 'Discount Amount'], axis = 1)
    df_order_cb=df_order_cb.rename(columns={'Invoice Number':'order_id','Customer Id':'customer_id','Subscription Id':'subscription_id',
                                        'Customer Email':'email','Amount':'total_price',
                                        'Invoice Date':'created_at','Name':'coupon','Plan Id':'plan_type',
                                        'Billing Province':'billing_province'})
    #concat orders data
    df_order = pd.concat([df_shop_orders, df_order_cb], ignore_index=True, sort=False)
    return df_order


def clean_subs(df_subs_shopify,df_subs_cb,df_offer_update,df_boxtype):
    """
    cleaning subs csv file
    """
    #SUBS DATA
    #Shopify
    df_subs_shopify=df_subs_shopify[['subscription_id','customer_id','created_at','customer_email','status','variant_title','recurring_price','cancelled_at','cancellation_reason','cancellation_reason_comments','product_title']]
    df_subs_shopify=df_subs_shopify.rename(columns={'customer_email':'email','status':'sub_status',
                                                    'variant_title':'plan_type','recurring_price':'plan_price','product_title':'product'})
    df_subs_shopify=pd.merge(df_subs_shopify,df_offer_update,on='subscription_id',how='left')
    #Chargebee
    df_subs_cb=df_subs_cb[['subscriptions.id','customers.id','subscriptions.created_at','customers.email','subscriptions.status',
                           'subscriptions.plan_id','subscriptions.plan_unit_price','subscriptions.cancelled_at',
                           'subscriptions.cancel_reason']]
    df_subs_cb=df_subs_cb.rename(columns={'subscriptions.id':'subscription_id','customers.id':'customer_id',
                                                    'subscriptions.created_at':'created_at','customers.email':'email'
                                          ,'subscriptions.status':'sub_status','subscriptions.plan_id':'plan_type',
                                          'subscriptions.plan_unit_price':'plan_price','subscriptions.cancelled_at':'cancelled_at',
                                         'subscriptions.cancel_reason':'cancellation_reason'})
    df_boxtype=df_boxtype.rename(columns={'subscriptions.id':'subscription_id'})
    df_subs_cb=pd.merge(df_subs_cb,df_boxtype,on='subscription_id',how='left')
    #concat subs data
    df_subs = pd.concat([df_subs_shopify, df_subs_cb], ignore_index=True, sort=False)
    return df_subs


if __name__ == "__main__":

    Path('data\\').mkdir(parents=True, exist_ok=True)

    df_order_1=pd.read_csv(ROOT_DIR + '/data/Raw/shopify_orders_1.csv')
    df_order_2=pd.read_csv(ROOT_DIR + '/data/Raw/shopify_orders_2.csv')
    df_shop_orders=pd.concat([df_order_1, df_order_2,], ignore_index=True, sort=False)
    df_subs_shopify=pd.read_csv(ROOT_DIR + '/data/Raw/subs_shopify.csv')
    df_order_cb=pd.read_csv(ROOT_DIR + '/data/Raw/Invoices.csv')
    df_subscoup_cb=pd.read_csv(ROOT_DIR + '/data/Raw/SubscriptionCoupons.csv')
    df_coup_type_cb=pd.read_csv(ROOT_DIR + '/data/Raw/Coupons.csv')
    df_subs_cb=pd.read_csv(ROOT_DIR + '/data/Raw/Subscriptions.csv')
    df_offer_update=pd.read_csv(ROOT_DIR + '/data/Raw/offer_type_update.csv',sep=';')
    df_boxtype=pd.read_csv(ROOT_DIR + '/data/Raw/chargebee_boxtype.csv',sep=';')

    #orders data
    df_orders=clean_orders(df_shop_orders,df_order_cb,df_subscoup_cb,df_coup_type_cb,df_subs_cb)
    #subs data
    df_subs=clean_subs(df_subs_shopify,df_subs_cb,df_offer_update,df_boxtype)

    #export data to cleaned
    df_orders.to_excel(ROOT_DIR + '/data/Cleaned/orders.xlsx',index=False)
    df_subs.to_excel(ROOT_DIR + '/data/Cleaned/subs.xlsx',index=False)
    print('Export Completed!')
