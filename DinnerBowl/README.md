# Dinner Bowl Cohort Report Data Extraction:
Extract & Clean Data from shopify csv file. This data will be used to update Dinner Bowl Cohort Report.

# Installation on Local Python env
* Clone repo
* Install requirements `pip install -r requirements.txt`
* Download csv's from Shopify and add to `data/Raw` folder in repo
* Rename shopify csv's to `customers.csv` & `subs_cancelled.csv`
* Run app `python data_clean.py`

# How to download Shopify CSV's?
* Download all orders from Shopify under orders tab.
* Download all subscribers from Shopify under customers tab.
* Download all subscribers - cancelled from recharge app

# Who do I talk to?
* brendan.potgieter@madpaws.com.au
* brendanpotgieter12@gmail.com
