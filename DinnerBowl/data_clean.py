import pandas as pd
import re
import docx
import os
from pathlib import Path
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(os.path.expanduser('~user'), '.env')
load_dotenv(dotenv_path)
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

def clean_orders(df_raw):
    """
    cleaning orders csv file
    """
    #ORDERS DATA
    #Shopify
    df_name=df_raw.groupby('Name')['Lineitem name'].agg(lambda x: x.tolist())
    df_name=pd.DataFrame(df_name)
    df_quantity=df_raw.groupby('Name')['Lineitem quantity'].agg(sum)
    df_quantity=pd.DataFrame(df_quantity)
    df_temp=df_raw.drop_duplicates(subset=['Name'])
    df_new=pd.merge(df_temp, df_name, left_on='Name', right_on='Name', how='left')
    df=pd.merge(df_new, df_quantity, left_on='Name', right_on='Name', how='left')
    df=df.rename(columns={'Lineitem name_y':'Item_List'})
    df=df.rename(columns={'Lineitem quantity_y':'Quantity_Total'})
    #Creating Filters
    df['Note Attributes']=df['Note Attributes'].apply(lambda x: str(x))
    df['Number_dogs']=df['Note Attributes'].apply(lambda x: re.search(r'Number of Dogs:(.*)\nDog 1',x).group(1) if pd.Series(x).str.contains(r'Number of Dogs').sum()>0 else "Unknown")
    df['Gender_dog1']=df['Note Attributes'].apply(lambda x: re.search(r'Gender:(.*)\nDog 1',x).group(1) if pd.Series(x).str.contains(r'Gender').sum()>0 else "Unknown")
    df['Gender_dog2']=df['Note Attributes'].apply(lambda x: re.search(r'Gender:(.*)\nDog 2',x).group(1) if (pd.Series(x).str.contains(r'Gender').sum()>0) & (pd.Series(x).str.contains(r'\nDog 2').sum()>0)  else "Only 1 dog")
    df['Breed_dog1']=df['Note Attributes'].apply(lambda x: re.search(r'Breeds:(.*)\nDog 1',x).group(1) if pd.Series(x).str.contains(r'Breeds').sum()>0 else "Unknown")
    df['Breed_dog2']=df['Note Attributes'].apply(lambda x: re.search(r'Breeds:(.*)\nDog 2',x).group(1) if (pd.Series(x).str.contains(r'Breeds').sum()>0) & (pd.Series(x).str.contains(r'\nDog 2').sum()>0)  else "Only 1 dog")
    #df['Age_dog1']=df['Note Attributes'].apply(lambda x: re.search(r'Age:(.*)\nDog 1',x).group(1) if pd.Series(x).str.contains(r'Age').sum()>0 else "Unknown")
    #df['Age_dog2']=df['Note Attributes'].apply(lambda x: re.search(r'Age:(.*)\nDog 2',x).group(1) if (pd.Series(x).str.contains(r'Age').sum()>0) & (pd.Series(x).str.contains(r'\nDog 2').sum()>0)  else "Only 1 dog")
    #df['Body_dog1']=df['Note Attributes'].apply(lambda x: re.search(r'Body Shape:(.*)\nDog 1',x).group(1) if pd.Series(x).str.contains(r'Body Shape').sum()>0 else "Unknown")
    #df['Body_dog2']=df['Note Attributes'].apply(lambda x: re.search(r'Body Shape:(.*)\nDog 2',x).group(1) if (pd.Series(x).str.contains(r'Body Shape').sum()>0) & (pd.Series(x).str.contains(r'\nDog 2').sum()>0)  else "Only 1 dog")
    df['Weight_dog1']=df['Note Attributes'].apply(lambda x: re.search(r'Weight:(.*)\nDog 1',x).group(1) if pd.Series(x).str.contains(r'Weight').sum()>0 else "Unknown")
    #df['Weight_dog2']=df['Note Attributes'].apply(lambda x: re.search(r'Weight:(.*)\nDog 2',x).group(1) if (pd.Series(x).str.contains(r'Weight').sum()>0) & (pd.Series(x).str.contains(r'\nDog 2').sum()>0)  else "Only 1 dog")
    df['Weight_dog1']=df['Weight_dog1'].apply(lambda x: float(x[:x.index("kg")]) if pd.Series(x).str.contains(r'kg').sum()>0 else 0)
    #df['Weight_dog2']=df['Weight_dog2'].apply(lambda x: float(x[:x.index("kg")]) if pd.Series(x).str.contains(r'kg').sum()>0 else 0)
    #df['Activity_dog1']=df['Note Attributes'].apply(lambda x: re.search(r'Activity Level:(.*)\nDog 1',x).group(1) if pd.Series(x).str.contains(r'Activity Level').sum()>0 else "Unknown")
    #df['Activity_dog2']=df['Note Attributes'].apply(lambda x: re.search(r'Activity Level:(.*)\nDog 2',x).group(1) if (pd.Series(x).str.contains(r'Activity Level').sum()>0) & (pd.Series(x).str.contains(r'\nDog 2').sum()>0)  else "Only 1 dog")
    df['UEats_dog1']=df['Note Attributes'].apply(lambda x: re.search(r'Usually Eats:(.*)\nDog 1',x).group(1) if pd.Series(x).str.contains(r'Usually Eats').sum()>0 else "Unknown")
    #df['UEats_dog2']=df['Note Attributes'].apply(lambda x: re.search(r'Usually Eats:(.*)\nDog 2',x).group(1) if (pd.Series(x).str.contains(r'Usually Eats').sum()>0) & (pd.Series(x).str.contains(r'\nDog 2').sum()>0)  else "Only 1 dog")
    #df['Puppy_dog1']=df['Note Attributes'].apply(lambda x: re.search(r'Is Puppy:(.*)\nDog 1',x).group(1) if pd.Series(x).str.contains(r'Is Puppy').sum()>0 else "Unknown")
    #df['Puppy_dog2']=df['Note Attributes'].apply(lambda x: re.search(r'Is Puppy:(.*)\nDog 2',x).group(1) if (pd.Series(x).str.contains(r'Is Puppy').sum()>0) & (pd.Series(x).str.contains(r'\nDog 2').sum()>0)  else "Only 1 dog")
    #df['Issues_dog1']=df['Note Attributes'].apply(lambda x: re.search(r'Issues:(.*)\nDog 1',x).group(1) if pd.Series(x).str.contains(r'Issues').sum()>0 else "Unknown")
    #df['Issues_dog2']=df['Note Attributes'].apply(lambda x: re.search(r'Issues:(.*)\nDog 2',x).group(1) if (pd.Series(x).str.contains(r'Issues:').sum()>0) & (pd.Series(x).str.contains(r'\nDog 2').sum()>0)  else "Only 1 dog")
    df=df[['Name','Email','Total','Discount Amount','Discount Code','Created at','Financial Status','UEats_dog1','Weight_dog1','Quantity_Total','Note Attributes','Tags','Item_List','Number_dogs','Gender_dog1','Gender_dog2','Breed_dog1','Breed_dog2']]
    return df

def clean_discounts(df_raw,df_orders):
    """
    cleaning orders raw csv file to create discount csv
    """
    df_discount=df_raw.groupby('Email')['Created at'].agg(min)
    df_discount=pd.DataFrame(df_discount)
    df_discount=pd.merge(df_orders, df_discount, left_on=['Email','Created at'], right_on=['Email','Created at'], how='inner')
    df_discount=df_discount.drop_duplicates(subset=['Email','Discount Code'])
    df_discount=df_discount[['Email','Discount Code']]
    df_discount=df_discount.dropna()
    return df_discount

def clean_fraud(df_fraud):
    """
    cleaning orders raw csv file to create fraud csv
    """
    df_fraud=df_fraud.drop_duplicates(subset=['Email'])
    df_fraud['fraud']=1
    return df_fraud

def clean_bowl(df_raw,df_orders):
    """
    cleaning orders raw csv file to create bowl csv
    """
    df_bowl=df_raw.groupby('Email')['Created at'].agg(min)
    df_bowl=pd.DataFrame(df_bowl)
    df_bowl=pd.merge(df_orders, df_bowl, left_on=['Email','Created at'], right_on=['Email','Created at'], how='inner')
    df_bowl=df_bowl[['Email','Tags']]
    return df_bowl

def clean_cancelled(df_cancel):
    """
    cleaning cancel raw csv file to create cancel reason csv
    """
    df_x=df_cancel.groupby('customer_id')['cancellation_reason'].agg(lambda x: x.tolist())
    df_x=pd.DataFrame(df_x)
    df_y=df_cancel.groupby('customer_id')['cancellation_reason_comments'].agg(lambda x: x.tolist())
    df_y=pd.DataFrame(df_y)
    df_z=df_cancel.groupby('customer_id')['product_title'].agg(lambda x: x.tolist())
    df_z=pd.DataFrame(df_z)
    df_cancel=df_cancel.drop_duplicates(subset=['customer_id'])
    df_cancel=pd.merge(df_cancel, df_x, left_on='customer_id', right_on='customer_id', how='inner')
    df_cancel=pd.merge(df_cancel, df_y, left_on='customer_id', right_on='customer_id', how='inner')
    df_cancel=pd.merge(df_cancel, df_z, left_on='customer_id', right_on='customer_id', how='inner')
    df_cancel=df_cancel[['customer_id','customer_email','status','cancelled_at','cancellation_reason_y','cancellation_reason_comments_y','product_title_y']]
    df_cancel=df_cancel.rename(columns={'cancellation_reason_y':'cancellation_reason'})
    df_cancel=df_cancel.rename(columns={'cancellation_reason_comments_y':'cancellation_reason_comments'})
    df_cancel=df_cancel.rename(columns={'product_title_y':'product_title'})
    df_cancel=df_cancel[['customer_id','customer_email','cancelled_at','cancellation_reason','cancellation_reason_comments']]
    return df_cancel


if __name__ == "__main__":

    Path('data\\').mkdir(parents=True, exist_ok=True)

    df_order_1=pd.read_csv(ROOT_DIR + '/data/Raw/orders_export_1.csv')
    df_order_2=pd.read_csv(ROOT_DIR + '/data/Raw/orders_export_2.csv')
    df_order_3=pd.read_csv(ROOT_DIR + '/data/Raw/orders_export_3.csv')
    df_order_4=pd.read_csv(ROOT_DIR + '/data/Raw/orders_export_4.csv')
    df_fraud=pd.read_csv(ROOT_DIR + '/data/Raw/fraud_report.csv')
    df_cancel=pd.read_csv(ROOT_DIR + '/data/Raw/subs_cancelled.csv')
    df_subs_1=pd.read_csv(ROOT_DIR + '/data/Raw/customers_1.csv')
    df_subs_2=pd.read_csv(ROOT_DIR + '/data/Raw/customers_2.csv')

    #concat orders 1-3
    df_raw = pd.concat([df_order_1, df_order_2, df_order_3, df_order_4], ignore_index=True, sort=False)
    df_raw = df_raw.drop_duplicates()

    #concat subs 1-2
    df_subs = pd.concat([df_subs_1, df_subs_2], ignore_index=True, sort=False)
    df_subs = df_subs.drop_duplicates()

    #clean data
    df_orders=clean_orders(df_raw)
    df_discount=clean_discounts(df_raw,df_orders)
    df_fraud=clean_fraud(df_fraud)
    df_bowl=clean_bowl(df_raw,df_orders)
    df_cancel=clean_cancelled(df_cancel)

    #export data to cleaned
    df_orders.to_excel(ROOT_DIR + '/data/Cleaned/unqiue_orders.xlsx',index=False)
    df_discount.to_excel(ROOT_DIR + '/data/Cleaned/discount.xlsx',index=False)
    df_fraud.to_excel(ROOT_DIR + '/data/Cleaned/unqiue_fraud.xlsx',index=False)
    df_bowl.to_excel(ROOT_DIR + '/data/Cleaned/bowl_type.xlsx',index=False)
    df_cancel.to_excel(ROOT_DIR + '/data/Cleaned/cancelled.xlsx',index=False)
    df_subs.to_excel(ROOT_DIR + '/data/Cleaned/customers.xlsx',index=False)
    print('Export Completed!')
